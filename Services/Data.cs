// Data.cs
// Holds all the information

using System.Collections.Generic;
using WeatherStation.Models;
using WeatherStationSensors;

namespace WeatherStation.Services {
    public class Data {
        public List<MeasurementItem> measurement_items;

        public Data() {
            GetSensorData();
        }
        public IEnumerable<MeasurementItem> GetItems() {
            GetSensorData();
            return measurement_items;
        }

        // Grab data from the sensors and put it in a new list
        public void GetSensorData() {
            measurement_items = new List<MeasurementItem>();
            Sensor[] sensors = new Sensor[] {new HTS221(0x5F), new LPS25H(0x5C)};
            // Iterate through the sensors
            foreach (Sensor curr_sensor in sensors) {
                // Iterate through the available measurements from the sensors
                foreach (Measurement curr_measurement in curr_sensor.AvailMeasurements()) {
                    measurement_items.Add(new MeasurementItem(curr_sensor, curr_measurement));
                }
            }
            // Sort the items
            MeasurementItemCompare comparator = new MeasurementItemCompare();
            measurement_items.Sort(comparator);
        }

        // Sort by measurement type then sensor name
        private class MeasurementItemCompare : IComparer<MeasurementItem> {
            public int Compare(MeasurementItem x, MeasurementItem y) {
                int name_compare = x.measurement.CompareTo(y.measurement);
                if (name_compare == 0) {
                    return x.sensor.CompareTo(y.sensor);
                } else {
                    return name_compare;
                }
            }
        }

    }
}