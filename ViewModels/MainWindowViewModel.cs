﻿// MainWindowViewModel.cs

using System;
using System.Collections.Generic;
using System.Text;
using WeatherStation.Services;
using System.Reactive.Linq;
using ReactiveUI;

namespace WeatherStation.ViewModels {
    public class MainWindowViewModel : ViewModelBase {
        public SummaryViewModel List { get; }

        private Data sensor_data;
        ViewModelBase content;

        public MainWindowViewModel(Data db) {
            sensor_data = db;
            content = List = new SummaryViewModel(db.GetItems());
        }

        public ViewModelBase Content {
            get => content;
            private set => this.RaiseAndSetIfChanged(ref content, value);
        }

        // Create a new Summary view whenever the Refresh button is pressed
        // Not the best solution but couldnt get the binding to work right
        public void Refresh() {
            Content = new SummaryViewModel(sensor_data.GetItems());
        }
    }
}
