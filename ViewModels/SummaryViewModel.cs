// SummaryViewModel.cs
// View model for summary screen

using System.Collections.Generic;
using System.Collections.ObjectModel;
using WeatherStation.Models;
using System.Reactive;
using ReactiveUI;

using WeatherStationSensors;

namespace WeatherStation.ViewModels {
    public class SummaryViewModel : ViewModelBase {
        public ObservableCollection<MeasurementItem> Items {get; set;}

        public SummaryViewModel(IEnumerable<MeasurementItem> items) {
            Items = new ObservableCollection<MeasurementItem>(items);
            //Refresh = ReactiveCommand.Create(RefreshSensorData);
        }

        // This is supposed to be able to refresh only the value fields but I can't get it to work
        // I'm either missing something or theres some weird problem going on
        // As an alternative, the Summary view gets recreated whenever the refresh button is pushed
        public ReactiveCommand<Unit, Unit> Refresh {get;}
        public void RefreshSensorData() {
            foreach (var item in Items) {
                item.GetValue();
            }
            this.RaisePropertyChanged("Items");
        }
    }
}