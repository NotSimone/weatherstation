using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace WeatherStation.Views
{
    public class SummaryView : UserControl
    {
        public SummaryView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}