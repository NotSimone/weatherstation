using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using WeatherStation.ViewModels;
using WeatherStation.Views;
using WeatherStation.Services;

namespace WeatherStation {
    public class App : Application {
        public override void Initialize() {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted() {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop) {
                // Create data instance and pass into MainWindowViewModel
                Data db = new Data();
                desktop.MainWindow = new MainWindow {
                    DataContext = new MainWindowViewModel(db)
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}