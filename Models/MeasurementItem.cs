using WeatherStationSensors;

namespace WeatherStation.Models {
    public class MeasurementItem {
        public string measurement {get;}
        private MeasurementType _measurement;
        public string sensor {get;}
        private Sensor _sensor;
        public string unit {get;}
        public string value {get; set;}

        // Half of these should probably be in the model view but its not a big deal
        public MeasurementItem(Sensor new_sensor, Measurement new_measurement) {
            _measurement = new_measurement.type;
            _sensor = new_sensor;
            measurement = new_measurement.type.ToString();
            sensor = new_sensor.name;
            unit = new_measurement.unit;
            GetValue();
        }

        // Refresh the value for the measurement
        public void GetValue() {
            value = _sensor.SenseData(_measurement).ToString("n2");
        }
    }
}