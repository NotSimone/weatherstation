# WeatherStation

## About

Graphical interface for sensors on the Raspberry Pi. Relies on my WeatherStationSensors library for grabbing data from the Sense Hat.

![screenshot](/screenshot.PNG)

Made with .Net Core 3.1 and [Avalonia](https://avaloniaui.net/). IO done with [RaspberryIO](https://unosquare.github.io/raspberryio/)

## Requirements

Raspberry Pi running some flavour of Linux. The default sensor config is based on the Sense Hat but you can change this easily yourself.

## Compilation

Requires .Net Core SDK 3.1.  

Run `dotnet publish` in the directory to automatically build with the csproj. WeatherStationSensors needs to be in the parent directory.
